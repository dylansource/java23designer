package com.surpass.designer.No11_组合模式.composemode;


import java.util.Iterator;

/**
 * 中餐厅菜单
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 23:39
 */
public class DinerMenu extends MenuComponent {
    private final static int Max_Item = 5; //最多5个菜单项
    public int numberOfItems = 0;
    private MenuComponent[] menuItems;

    public DinerMenu() {
        menuItems = new MenuComponent[Max_Item];
        addItem("vegetable Blt", "bacon&lettuce&tomato&cabbage", true, 3.58f);
        addItem("Blt", "bacon&lettuce&tomato", false, 3.00f);
        addItem("bean soup", "bean&potato salad", true, 3.28f);
        addItem("hotdog", "onions&cheese&bread", false, 3.05f);
        addSubMenu(new SubMenu());
    }

    private void addItem(String name, String description, boolean vegetable,
                         float price) {
        MenuComponent menuItem = new MenuItem(name, description, vegetable, price);
        if (numberOfItems >= Max_Item) {
            System.err.println("sorry,menu is full! can not add another item");
        }else {
            menuItems[numberOfItems] = menuItem;
            numberOfItems++;
        }

    }

    private void addSubMenu(MenuComponent mMenuComponent) {
        if (numberOfItems >= Max_Item) {
            System.err.println("sorry,menu is full!can not add another item");
        } else {
            menuItems[numberOfItems] = mMenuComponent;
            numberOfItems++;
        }
    }

    @Override
    public Iterator getIterator() {
        return new ComposeIterator(new DinerIterator());
    }

    class DinerIterator implements Iterator {
        private int position;

        public DinerIterator() {
            position = 0;
        }
        @Override
        public boolean hasNext() {
            if (position < numberOfItems) {
                return true;
            }
            return false;
        }

        @Override
        public Object next() {
            MenuComponent menuItem = menuItems[position];
            position++;
            return menuItem;
        }
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public float getPrice() {
        return super.getPrice();
    }

    @Override
    public String getDescription() {
        return super.getDescription();
    }


    //主菜单输出
    @Override
    public void print() {
        System.out.println("***This is DinerMenu***");

    }
}
