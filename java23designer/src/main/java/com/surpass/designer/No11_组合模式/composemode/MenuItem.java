package com.surpass.designer.No11_组合模式.composemode;

/**
 * 菜单项
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 23:33
 */
public class MenuItem extends MenuComponent {
    private String name, description;
    private boolean vegetable;
    private float price;

    public MenuItem(String name, String description, boolean vegetable, float price) {
        this.name = name;
        this.description = description;
        this.vegetable = vegetable;
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public float getPrice() {
        return price;
    }

    @Override
    public boolean isVegetable() {
        return vegetable;
    }

    //主菜单的子菜单项输出
    @Override
    public void print() {
        System.out.println(getName() + "***" + getPrice() + "***" + getDescription());
    }
}
