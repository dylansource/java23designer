package com.surpass.designer.No11_组合模式.composemode;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * 子菜单
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 23:35
 */
public class SubMenu extends MenuComponent {
	//菜单项也是超类类型
	private ArrayList<MenuComponent> menuItems;

	public SubMenu() {
		menuItems = new ArrayList<>();

		addItem("Apple Cookie", "Apple&candy&Cookie", true, 1.99f);
		addItem("Banana Cookie", "Banana&candy&Cookie", false, 1.59f);
		addItem("Orange Cookie", "Orange&Cookie", true, 1.29f);

	}

	private void addItem(String name, String description, boolean vegetable, float price) {
		MenuItem menuItem = new MenuItem(name, description, vegetable, price);
		menuItems.add(menuItem);
	}

	@Override
	public Iterator getIterator() {
		return new ComposeIterator(menuItems.iterator());
	}

	//主菜单输出
	@Override
	public void print() {
		System.out.println("****This is SubMenu****");
	}
}
