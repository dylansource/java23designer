package com.surpass.designer.No8_外观模式.facademode.hometheater;

/**
 * 灯光
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 16:23
 */
public class TheaterLights {
    private static TheaterLights instance = null;

    private TheaterLights(){}

    public static TheaterLights getInstance() {
        if (instance == null) {
            instance = new TheaterLights();
        }
        return instance;
    }

    public void on(){
        System.out.println("TheaterLights On");
    }

    public void off(){
        System.out.println("TheaterLights Off");
    }

    //调暗
    public void dim(int d) {
        System.out.println("TheaterLights dim to " + d + "%");
    }

    //调亮
    public void bright() {
        dim(100);
        System.out.println("TheaterLights bright");
    }

}
