package com.surpass.designer.No8_外观模式.facademode.hometheater;

/**
 * 外观模式
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 16:34
 */
public class HomeTheaterFacade {
    private TheaterLights mTheaterLights; //灯光
    private Popcorn mPopcorn; //爆米花机
    private Stereo mStereo; //音响
    private Projector mProjector; //投影仪
    private Screen mScreen; //屏幕
    private DVDPlayer mDVDPlayer; //播放器

    public HomeTheaterFacade() {
        mTheaterLights = TheaterLights.getInstance();
        mPopcorn = Popcorn.getInstance();
        mStereo = Stereo.getInstance();
        mProjector = Projector.getInstance();
        mScreen = Screen.getInstance();
        mDVDPlayer = DVDPlayer.getInstance();
    }

    //一次实现多个,类似于宏命令
    public void ready() {
        mPopcorn.on();
        mPopcorn.pop();
        mScreen.down();
        mProjector.on();
        mStereo.on();
        mDVDPlayer.on();
        mDVDPlayer.setdvd();
        mTheaterLights.dim(10);
    }

    public void end() {
        mPopcorn.off();
        mTheaterLights.bright();
        mScreen.up();
        mProjector.off();
        mStereo.off();

        mDVDPlayer.setdvd();
        mDVDPlayer.off();
    }

    //针对单个对象,单一功能
    public void play() {
        mDVDPlayer.play();
    }

    public void pause() {
        mDVDPlayer.pause();
    }
}
