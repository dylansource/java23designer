package com.surpass.designer.No8_外观模式.facademode.hometheater;

/**
 * 屏幕
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 16:23
 */
public class Screen {
    private static Screen instance = null;

    private Screen(){}

    public static Screen getInstance() {
        if (instance == null) {
            instance = new Screen();
        }
        return instance;
    }

    public void up(){
        System.out.println("Screen up");
    }

    public void down(){
        System.out.println("Screen down");
    }


}
