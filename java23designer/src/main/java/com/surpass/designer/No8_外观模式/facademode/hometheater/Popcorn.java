package com.surpass.designer.No8_外观模式.facademode.hometheater;

/**
 * 爆米花机
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 16:23
 */
public class Popcorn {
    private static Popcorn instance = null;

    private Popcorn(){}

    public static Popcorn getInstance() {
        if (instance == null) {
            instance = new Popcorn();
        }
        return instance;
    }

    public void on(){
        System.out.println("Popcorn On");
    }

    public void off(){
        System.out.println("Popcorn Off");
    }

    public void pop(){
        System.out.println("Popcorn is popping");
    }

}
