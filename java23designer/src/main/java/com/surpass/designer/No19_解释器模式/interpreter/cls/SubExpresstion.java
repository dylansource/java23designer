package com.surpass.designer.No19_解释器模式.interpreter.cls;

import java.util.HashMap;

/**
 * -
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/7 11:48
 */
public class SubExpresstion extends SymbolExpresstion{

    public SubExpresstion(AbstractExpresstion _left, AbstractExpresstion _right) {
        super(_left, _right);
    }

    @Override
    public Float interpreter(HashMap<String, Float> var) {
        return super.left.interpreter(var) - super.right.interpreter(var);
    }
}
