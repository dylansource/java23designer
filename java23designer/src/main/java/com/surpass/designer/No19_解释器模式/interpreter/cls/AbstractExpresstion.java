package com.surpass.designer.No19_解释器模式.interpreter.cls;

import java.util.HashMap;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/7 11:40
 */
public abstract class AbstractExpresstion {
    //一列数据,k:字段信息
    public abstract Float interpreter(HashMap<String, Float> var);
}
