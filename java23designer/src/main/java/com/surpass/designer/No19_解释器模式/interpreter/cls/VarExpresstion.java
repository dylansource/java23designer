package com.surpass.designer.No19_解释器模式.interpreter.cls;

import java.util.HashMap;

/**
 * 变量表达式
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/7 11:42
 */
public class VarExpresstion extends AbstractExpresstion{
    private String key; //字段信息

    public VarExpresstion(String _key) {
        this.key = _key;
    }

    @Override
    public Float interpreter(HashMap<String, Float> var) {
        return var.get(this.key);

    }
}
