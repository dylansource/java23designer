package com.surpass.designer.No19_解释器模式.interpreter.cls;

import java.util.HashMap;

/**
 * 乘
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/7 11:45
 */
public class MultiExpresstion extends SymbolExpresstion{

    public MultiExpresstion(AbstractExpresstion _left, AbstractExpresstion _right) {
        super(_left, _right);
    }

    @Override
    public Float interpreter(HashMap<String, Float> var) {
        //左表达式*右表达式
        return super.left.interpreter(var) * super.right.interpreter(var);
    }
}
