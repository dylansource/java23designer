package com.surpass.designer.No19_解释器模式.interpreter.cls;

import java.util.HashMap;

/**
 * 符号表达式
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/7 11:43
 */
public abstract class SymbolExpresstion extends AbstractExpresstion{
    protected AbstractExpresstion left;
    protected AbstractExpresstion right;

    public SymbolExpresstion(AbstractExpresstion _left,
                             AbstractExpresstion _right) {
        this.left = _left;
        this.right = _right;
    }
}
