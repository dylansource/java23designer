package com.surpass.designer.No22_原型模式.protomode;

/**
 * 事件模板
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 21:48
 */
public class EventTemplate {
    //标题,内容
    private String eventSubject,eventContent;

    public EventTemplate(String eventSubject, String eventContent) {
        this.eventSubject = eventSubject;
        this.eventContent = eventContent;
    }

    public String getEventSubject() {
        return eventSubject;
    }

    public String getEventContent() {
        return eventContent;
    }
}
