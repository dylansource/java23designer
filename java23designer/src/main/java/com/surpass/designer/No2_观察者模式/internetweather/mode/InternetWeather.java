package com.surpass.designer.No2_观察者模式.internetweather.mode;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 19:20
 */
public class InternetWeather {
    public static void main(String[] args) {
        CurrentConditions mCurrentConditions;
        ForcastConditions mForcastConditions;
        WeatherDataSt mWeatherDataSt;

        mWeatherDataSt = new WeatherDataSt();
        mCurrentConditions = new CurrentConditions();
        mForcastConditions = new ForcastConditions();

        //注册观察者
        mWeatherDataSt.registrObserver(mCurrentConditions);
        mWeatherDataSt.registrObserver(mForcastConditions);

        mWeatherDataSt.setData(30, 150, 40);
        mWeatherDataSt.removeObserver(mCurrentConditions);
        mWeatherDataSt.setData(40,250,50);

    }
}
