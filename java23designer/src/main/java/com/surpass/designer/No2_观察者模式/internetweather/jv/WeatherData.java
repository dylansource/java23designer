package com.surpass.designer.No2_观察者模式.internetweather.jv;

import java.util.Observable;

/**
 * java内置观察者
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 21:17
 */
public class WeatherData extends Observable{
    private float mTemperature; //温度
    private float mPressure; //气压
    private float mHumidity; //湿度

    public float getmTemperature() {
        return mTemperature;
    }

    public float getmPressure() {
        return mPressure;
    }

    public float getmHumidity() {
        return mHumidity;
    }

    public void dataChange() {
        this.setChanged();
        this.notifyObservers(new Data(getmTemperature(),getmPressure(),getmHumidity()));
    }

    //模拟气象站传输数据
    public void setData(float mTemperature, float mPressure, float mHumidity) {
        this.mTemperature = mTemperature;
        this.mPressure = mPressure;
        this.mHumidity = mHumidity;
        dataChange();
    }

    //专门放参数的类
    public class Data {
        public float mTemperature;
        public float mPressure;
        public float mHumidity;
        public Data(float mTemperature, float mPressure, float mHumidity) {
            this.mTemperature = mTemperature;
            this.mPressure = mPressure;
            this.mHumidity = mHumidity;
        }
    }

}
