package com.surpass.designer.No2_观察者模式.internetweather;

/**
 * 公告板传入WeathData作为参数,有新信息,通知公告板update
 * 存在问题:新需求:下个礼拜天气预报公告板,则需要在WeathData新建新公告板
 *
 * 其他第三方公司接入气象站获取数据问题
 * 无法在运行时动态添加第三方
 * 扩展性差
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 18:43
 */
public class InternetWeather {
    public static void main(String[] args) {
        CurrentConditions mCurrentConditions;
        WeatherData mWeatherData;

        mCurrentConditions = new CurrentConditions();
        mWeatherData = new WeatherData(mCurrentConditions);
        mWeatherData.setData(30, 150, 40);

    }
}
