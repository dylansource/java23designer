package com.surpass.designer.No2_观察者模式.internetweather;

/**
 * 公告板
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 18:33
 */
public class CurrentConditions {
    private float mTemperature;
    private float mPressure;
    private float mHumidity;

    public void update(float mTemperature, float mPressure, float mHumidity){
        this.mTemperature = mTemperature;
        this.mPressure = mPressure;
        this.mHumidity = mHumidity;
        display();
    }

    //公告板打印内容
    public void display(){
        System.out.println("***Today mTemperature: " + mTemperature + "***");
        System.out.println("***Today mPressure: " + mPressure + "***");
        System.out.println("***Today mHumidity: " + mHumidity + "***");
    }

}
