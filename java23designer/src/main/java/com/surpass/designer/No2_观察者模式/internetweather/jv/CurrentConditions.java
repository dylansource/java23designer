package com.surpass.designer.No2_观察者模式.internetweather.jv;

import java.util.Observable;
import java.util.Observer;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 21:23
 */
public class CurrentConditions implements Observer {

    private float mTemperature; //温度
    private float mPressure; //气压
    private float mHumidity; //湿度

    @Override
    public void update(Observable o, Object arg) {
        this.mTemperature = ((WeatherData.Data) (arg)).mTemperature;
        this.mPressure = ((WeatherData.Data) (arg)).mPressure;
        this.mHumidity = ((WeatherData.Data) (arg)).mHumidity;
        display();
    }

    //公告板打印内容
    public void display(){
        System.out.println("***Today mTemperature: " + mTemperature + "***");
        System.out.println("***Today mPressure: " + mPressure + "***");
        System.out.println("***Today mHumidity: " + mHumidity + "***");
    }
}
