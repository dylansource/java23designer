package com.surpass.designer.No2_观察者模式.internetweather.jv;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 19:20
 */
public class InternetWeather {
    public static void main(String[] args) {
        CurrentConditions mCurrentConditions;
        ForcastConditions mForcastConditions;
        WeatherData mWeatherData;

        mCurrentConditions = new CurrentConditions();
        mForcastConditions = new ForcastConditions();
        mWeatherData = new WeatherData();

        //通知顺序不一样,java内置观察者先进后出
        mWeatherData.addObserver(mCurrentConditions);
        mWeatherData.addObserver(mForcastConditions);
        mWeatherData.setData(30,160,40);

        mWeatherData.deleteObserver(mCurrentConditions);
        mWeatherData.setData(40,111,77);

    }
}
