package com.surpass.designer.No18_蝇量模式.flyweight.fly;

import java.util.HashMap;

/**
 * 计划工厂
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/7 10:20
 */
public class PlantFactory {
    private HashMap<Integer, Plant> plantMap = new HashMap<>();

    public PlantFactory() {

    }

    public Plant getPlant(int type) {
        if (!plantMap.containsKey(type)) {
            switch (type) {
                case 0:
                    plantMap.put(0, new Tree());
                    break;
                case 1:
                    plantMap.put(1, new Grass());
                    break;
            }
        }
        return plantMap.get(type);
    }
}
