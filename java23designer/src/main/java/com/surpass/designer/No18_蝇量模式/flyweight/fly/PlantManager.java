package com.surpass.designer.No18_蝇量模式.flyweight.fly;

/**
 * 管理外部状态
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/7 10:23
 */
public class PlantManager {
    private int length = 10000000;
    private int[] xArray = new int[length]
            ,yArray = new int[length],
            AgeArray = new int[length],
            typeArray = new int[length]; //类型,该位置是树还是草

    private PlantFactory mPlantFactory;

    public PlantManager() {
        mPlantFactory = new PlantFactory();
        for (int i = 0; i < length; i++) {
            xArray[i] = (int) (Math.random() * length);
            yArray[i] = (int) (Math.random() * length);
            AgeArray[i] = (int) (Math.random() * length) % 5;
            typeArray[i] = (int) (Math.random() * length) % 2;
        }
    }

    public void displayTrees() {
        for (int i = 0; i < length; i++) {
            mPlantFactory.getPlant(typeArray[i]).display(xArray[i], yArray[i], AgeArray[i]);
        }
    }
}
