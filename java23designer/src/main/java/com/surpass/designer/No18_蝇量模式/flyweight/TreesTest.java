package com.surpass.designer.No18_蝇量模式.flyweight;

/**
 * 产生大量树对象
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 21:58
 */
public class TreesTest {
    private int length = 10000000;
    private Tree[] treelst = new Tree[length];

    public TreesTest() {
        for (int i = 0; i < length; i++) {
            treelst[i] = new Tree((int)(Math.random() * length),
                    (int)(Math.random() * length),
                    (int)(Math.random() * length) % 5);
        }
    }

    public void display() {
        for (int i = 0; i < treelst.length; i++) {
            treelst[i].display();
        }
    }
}
