package com.surpass.designer.No18_蝇量模式.flyweight.fly;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/7 10:18
 */
public abstract class Plant {

    public Plant() {

    }

    public abstract void display(int xCrood, int yCrood, int age);
}
