package com.surpass.designer.No18_蝇量模式.flyweight.ms;

/**
 * 管理蝇量模式下树的外部状态
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 22:10
 */
public class TreeManager {
    private int length = 10000000;

    //通过数组管理外部状态
    int[] xArray = new int[length];
    int[] yArray = new int[length];
    int[] AgeArray = new int[length];

    private TreeFlyWeight mTreeFlyWeight;

    public TreeManager() {
        mTreeFlyWeight = new TreeFlyWeight();
        for (int i = 0; i < length; i++) {
            xArray[i] = (int) (Math.random() * length);
            yArray[i] = (int) (Math.random() * length);
            AgeArray[i] = (int) (Math.random() * length) % 5;
        }
    }

    public void displayTrees() {
        for (int i = 0; i < length; i++) {
            mTreeFlyWeight.display(xArray[i], yArray[i], AgeArray[i]);
        }
    }
}
