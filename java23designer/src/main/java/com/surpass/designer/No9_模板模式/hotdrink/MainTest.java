package com.surpass.designer.No9_模板模式.hotdrink;

/**
 * 项目框架设计
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:28
 */
public class MainTest {
    public static void main(String[] args) {
//        HotDrink mCoffee = new Coffee();
//        HotDrink mTea = new Tea();
//        mCoffee.prepareRecipe();//方法通过子类实现了
//        mTea.prepareRecipe();
        TeaWithHook mTeaWithHook = new TeaWithHook();
        mTeaWithHook.prepareRecipe();

    }
}
