package com.surpass.designer.No9_模板模式.templatemode;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:15
 */
public class MainTest {
    public static void main(String[] args) {
        Coffee mCoffee = new Coffee();
        Tea tea = new Tea();
        mCoffee.prepareRecipe();
        tea.prepareRecipe();
    }
}
