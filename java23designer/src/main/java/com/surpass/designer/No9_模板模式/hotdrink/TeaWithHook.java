package com.surpass.designer.No9_模板模式.hotdrink;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:44
 */
public class TeaWithHook extends HotDrinkTemplate{

    @Override
    public void addCondiments() {
        System.out.println("Adding lemon");
    }

    @Override
    public void brew() {
        System.out.println("Brewing Tea");
    }

    @Override
    public boolean wantCondimentsHook() {
        System.out.println("Condiments,yes or no?please input(y/n):");
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String result = "";
        try {
            result = in.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (result.equals("n")) {
            return false;
        }
        return true;
    }
}
