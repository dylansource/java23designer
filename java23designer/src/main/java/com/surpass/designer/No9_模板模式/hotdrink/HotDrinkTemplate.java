package com.surpass.designer.No9_模板模式.hotdrink;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:36
 */
public abstract class HotDrinkTemplate {

    //模板步骤
    public final void prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        if (wantCondimentsHook()) {
            addCondiments();
        }else{
            System.out.println("No Condiments");
        }

    }

    //钩子函数,是否要放佐料,根据实际情况确定
    public boolean wantCondimentsHook() {
        return true;
    }

    public final void boilWater() {
        System.out.println("Boiling water");
    }

    public final void pourInCup() {
        System.out.println("Pouring into cup");
    }

    public abstract void addCondiments();

    public abstract void brew();
}
