package com.surpass.designer.No9_模板模式.hotdrink;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:23
 */
public abstract class HotDrink {
    //步骤固定
    public final void prepareRecipe() {
        boilWater();
        brew();
        pourInCup();
        addCondiments();
    }

    public final void boilWater() {
        System.out.println("Boiling water");
    }

    public abstract void brew();

    public final void pourInCup() {
        System.out.println("Pouring into cup");
    }

    public abstract void addCondiments();
}
