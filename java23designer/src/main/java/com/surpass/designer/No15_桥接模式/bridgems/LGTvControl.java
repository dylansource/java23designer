package com.surpass.designer.No15_桥接模式.bridgems;

import com.surpass.designer.No15_桥接模式.bridgems.control.LGControl;
import com.surpass.designer.No15_桥接模式.bridgems.control.TvControl;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 0:01
 */
public class LGTvControl extends LGControl implements TvControl {
    private static int ch = 0;
    private static boolean ison = false;
    @Override
    public void Onoff() {
        if (ison) {
            ison = false;
            super.Off();
        } else {
            ison = true;
            super.On();
        }
    }

    @Override
    public void nextChannel() {
        ch++;
        super.setChannel(ch);
    }

    @Override
    public void preChannel() {
        ch--;
        if (ch < 0) {
            ch = 200;
        }
        super.setChannel(ch);
    }
}
