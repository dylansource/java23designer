package com.surpass.designer.No15_桥接模式.bridgems.control;

/**
 * 厂家提供接口
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/3 23:48
 */
public interface Control {
    public void On();
    public void Off();
    public void setChannel(int ch);
    public void setVolume(int vol);
}
