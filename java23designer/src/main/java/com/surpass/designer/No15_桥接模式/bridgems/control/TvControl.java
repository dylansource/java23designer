package com.surpass.designer.No15_桥接模式.bridgems.control;

/**
 * 自己接口
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/3 23:53
 */
public interface TvControl {
    public void Onoff(); //开关一个按钮
    public void nextChannel();
    public void preChannel();
}
