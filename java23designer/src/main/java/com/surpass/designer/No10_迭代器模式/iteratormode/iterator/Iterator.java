package com.surpass.designer.No10_迭代器模式.iteratormode.iterator;

/**
 * 迭代器接口
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 21:11
 */
public interface Iterator {
    public boolean hasNext();
    public Object next();
}
