package com.surpass.designer.No10_迭代器模式.iteratormode;

/**
 * 传统设计方案:
 * 1.扩展性差:新增一餐厅菜单使用HashTable维护,则必须新增类,暴露菜单
 * 2.耦合度增加(女招待,如果菜单变更,则打印菜单方法也变更)
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/25 23:39
 */
public class MainTest {
    public static void main(String[] args) {

        Watress mWatress = new Watress();

        mWatress.printMenu();

    }

}
