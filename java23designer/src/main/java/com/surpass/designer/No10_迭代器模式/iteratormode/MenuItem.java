package com.surpass.designer.No10_迭代器模式.iteratormode;

/**
 * 迭代器模式:两个菜单项目合并
 * 一个项目用ArrayList管理菜单,一个项目用数组管理菜单
 * 现要合并成一个新项目:有两个选择
 * 1.重新开发
 * 2.使用已有项目
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/25 23:15
 */
public class MenuItem {
    private String name,description;
    private boolean vegetable;//素食
    private float price;

    public MenuItem(String name, String description, boolean vegetable, float price) {
        this.name = name;
        this.description = description;
        this.vegetable = vegetable;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isVegetable() {
        return vegetable;
    }

    public float getPrice() {
        return price;
    }
}
