package com.surpass.designer.No10_迭代器模式.iteratormode.iterator;

import com.surpass.designer.No10_迭代器模式.iteratormode.MenuItem;

import java.util.ArrayList;

/**
 * 迭代器iterators集合,一个餐厅一个迭代器
 * 通过集合进行解耦,修改餐厅类不影响Watiress类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 21:12
 */
public class Waitress {
    private ArrayList<Iterator> iterators = new ArrayList<>();

    public Waitress() {

    }

    public void addIterator(Iterator iterator) {
        iterators.add(iterator);
    }

    //打印菜单
    public void printMenu() {
        Iterator iterator;
        MenuItem menuItem;
        for(int i = 0;i< iterators.size();i++) {
            iterator = iterators.get(i);
            while (iterator.hasNext()) {
                menuItem = (MenuItem) iterator.next();
                System.out.println(menuItem.getName() + "***" +
                        menuItem.getPrice() + "***" + menuItem.getDescription());
            }
        }
    }
}
