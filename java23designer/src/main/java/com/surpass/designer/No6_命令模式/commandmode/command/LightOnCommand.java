package com.surpass.designer.No6_命令模式.commandmode.command;

import com.surpass.designer.No6_命令模式.commandmode.device.Light;

/**
 * 开电灯命令封装成一个对象
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 22:55
 */
public class LightOnCommand implements Command {

    private Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.On();
    }

    @Override
    public void undo() {
        light.Off();
    }
}
