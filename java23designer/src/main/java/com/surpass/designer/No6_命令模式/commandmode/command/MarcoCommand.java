package com.surpass.designer.No6_命令模式.commandmode.command;

/**
 * 宏命令,一个命令控制多个家电
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 23:42
 */
public class MarcoCommand implements Command {

    private Command[] commands;

    public MarcoCommand(Command[] commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        for (int i = 0; i < commands.length; i++) {
            commands[i].execute();
        }
    }

    @Override
    public void undo() {
        for (int i = commands.length - 1; i >= 0; i--) {
            commands[i].undo();
        }
    }
}
