package com.surpass.designer.No6_命令模式.commandmode.device;

/**
 * 灯
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 22:15
 */
public class Light {
    String loc = "";
    public Light(String loc){
        this.loc = loc;
    }

    public void On(){
        System.out.println(loc + " On");
    }
    public void Off(){
        System.out.println(loc + " Off");
    }
}
