package com.surpass.designer.No6_命令模式.commandmode.command;

import com.surpass.designer.No6_命令模式.commandmode.device.Light;

/**
 * 关电灯命令封装成一个对象
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 22:55
 */
public class LightOffCommand implements Command {

    private Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        light.Off();
    }

    @Override
    public void undo() {
        light.On();
    }
}
