package com.surpass.designer.No6_命令模式.commandmode;

import com.surpass.designer.No6_命令模式.commandmode.device.Light;
import com.surpass.designer.No6_命令模式.commandmode.device.Stereo;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 22:29
 */
public class ControlTest {
    public static void main(String[] args) {
        Control ctl;
        Light light = new Light("Bedroom");
        Stereo stereo = new Stereo();
        ctl = new TraditionControl(light, stereo);

        ctl.onButton(0);//打开灯
        ctl.offButton(0);//关闭灯
        ctl.onButton(1);//打开音响
        ctl.onButton(2);//增加音量
        ctl.offButton(2);//减小音量

        ctl.offButton(1);//关闭音响
    }
}
