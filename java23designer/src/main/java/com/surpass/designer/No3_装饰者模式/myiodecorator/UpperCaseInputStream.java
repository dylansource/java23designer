package com.surpass.designer.No3_装饰者模式.myiodecorator;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * 扩展java,小写字符输入,全部变成大写
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 12:25
 */
public class UpperCaseInputStream extends FilterInputStream{

    protected UpperCaseInputStream(InputStream in) {
        super(in);
    }

    //单字符读
    public int read() throws IOException {
        int c = super.read();
        return c == -1 ? c : Character.toUpperCase((char) c);
    }

    public int read(byte[] b,int offset,int len) throws IOException {
        int result = super.read(b, offset, len);
        for(int i = 0;i<result;i++) {
            b[i] = (byte)Character.toUpperCase((char) (b[i]));
        }
        return result;
    }

}
