package com.surpass.designer.No3_装饰者模式.coffeebar;

import com.surpass.designer.No3_装饰者模式.coffeebar.coffee.Decaf;
import com.surpass.designer.No3_装饰者模式.coffeebar.coffee.LongBlack;
import com.surpass.designer.No3_装饰者模式.coffeebar.decorator.Chocolate;
import com.surpass.designer.No3_装饰者模式.coffeebar.decorator.Milk;

/**
 * 采用装饰者模式:实现不同单品价格结合主体递归计算出结果
 * 新增调料类与单品类均不影响原有代码
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 22:29
 */
public class CoffeeBar {
    public static void main(String[] args) {
        Drink order;
        order = new Decaf();//单品咖啡

        System.out.println("order1 price:" + order.cost());
        System.out.println("order1 desc:" + order.getDescription());

        System.out.println("==========================");

        order = new LongBlack();//单品咖啡
        order = new Milk(order);//加牛奶
        order = new Chocolate(order);//加巧克力
        order = new Chocolate(order);//再加巧克力

        System.out.println("order2 price:" + order.cost());
        System.out.println("order2 desc:" + order.getDescription());

    }
}
