package com.surpass.designer.No3_装饰者模式.myiodecorator;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 12:30
 */
public class InputTest {
    public static void main(String[] args) {
        int c;
        try {
            InputStream in = new  UpperCaseInputStream(new BufferedInputStream(
                    new FileInputStream("C:\\Users\\Surpass\\Desktop\\tmp\\2222.txt")
            ));
            while ((c = in.read()) >= 0) {
                System.out.println((char) c);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
