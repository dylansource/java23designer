package com.surpass.designer.No3_装饰者模式.coffeebar.coffee;

import com.surpass.designer.No3_装饰者模式.coffeebar.Drink;

/**
 * 咖啡类继承饮料,重写cost方法
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 22:14
 */
public class Coffee extends Drink {

    @Override
    public float cost() {
        return super.getPrice();
    }
}
