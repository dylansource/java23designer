package com.surpass.designer.No1_策略模式.stimulateduck.oo;

/**
 * 超类挖的一个坑,每个子类都要填,增加工作量
 * 复杂度O(N^2),不是好的设计方式
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 17:04
 */
public abstract class Duck {
    public Duck(){}
    public void Quack(){
        System.out.println("--gaga--");
    }

    public abstract void display();

    public void swim(){
        System.out.println("--im swim--");
    }

    /**
     *  新需求,添加会飞的鸭子
     *  该方法使得所有子类全部会飞,不科学
     *  所有子类没有该该功能的覆盖该方法,重复劳动
     */
    public void Fly(){
        System.out.println("--im fly--");
    }

}
