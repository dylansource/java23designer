package com.surpass.designer.No17_责任链模式.chainms;

/**
 * 总裁
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 21:12
 */
public class PresidentApprover extends Approver{

    public PresidentApprover(String Name) {
        super(Name + " PresidentApprover");
    }

    @Override
    public void ProcessRequest(PurchaseRequest request) {
        if (request.GetSum() >= 50000) {
            System.out.println("**This request " + request.GetID()
                    + " will be handled by " +
                    this.Name);
        } else {
            successor.ProcessRequest(request);
        }
    }
}
