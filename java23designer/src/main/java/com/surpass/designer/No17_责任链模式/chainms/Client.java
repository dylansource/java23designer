package com.surpass.designer.No17_责任链模式.chainms;

/**
 * 员工,请求发起者
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 21:14
 */
public class Client {
    public Client() {

    }

    public PurchaseRequest sendRequest(int Type, int Number, float Price) {
        return new PurchaseRequest(Type, Number, Price);
    }
}
