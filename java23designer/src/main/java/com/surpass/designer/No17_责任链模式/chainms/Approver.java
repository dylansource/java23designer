package com.surpass.designer.No17_责任链模式.chainms;

/**
 * 请求批准者
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 21:02
 */
public abstract class Approver {
    Approver successor; //后续处理者
    String Name;

    public Approver(String Name) {
        this.Name = Name;
    }

    public abstract void ProcessRequest(PurchaseRequest request);

    public void SetSuccessor(Approver successor) {
        this.successor = successor;
    }
}
