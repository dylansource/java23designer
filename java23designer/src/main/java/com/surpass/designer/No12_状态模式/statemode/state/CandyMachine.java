package com.surpass.designer.No12_状态模式.statemode.state;

/**
 * 设计采用状态模式
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 21:36
 */
public class CandyMachine {
    State mSoldOutState;
    State mOnReadyState;
    State mHasCoin;
    State mSoldState;
    State mWinnerState;
    private State state;
    private int count = 0; //糖果个数

    public CandyMachine(int count) {
        this.count = count;
        mSoldOutState = new SoldOutState(this);
        mOnReadyState = new OnReadyState(this);
        mHasCoin = new HasCoin(this);
        mSoldState = new SoldState(this);
        mWinnerState = new WinnerState(this);
        if (count > 0) {
            state = mOnReadyState; //待机状态
        } else {
            state = mSoldOutState; //售罄状态
        }
    }

    public void setState(State state) {
        this.state = state;
    }

    public void insertCoin() {
        state.insertCoin();
    }

    public void returnCoin() {
        state.returnCoin();
    }

    //封装转动把手后的结果,可能有糖果掉出,可能提示没有硬币
    public void turnCrank() {
        //这一步可能修改了状态
        state.trunCrank();
        state.dispense();
        //两个state状态有可能不一样,返回不一样的结果
    }

    void releaseCandy() {
        if (count > 0) {
            count = count - 1;
            System.out.println("一个糖果正在掉出");
        }
    }

    public int getCount() {
        return count;
    }

    public void printstate() {
        state.printstate();
    }
}
