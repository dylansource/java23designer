package com.surpass.designer.No12_状态模式.statemode.state;

/**
 * 状态模式测试主类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 22:19
 */
public class MainTest {
    public static void main(String[] args) {
        CandyMachine mCandyMachine = new CandyMachine(6);

        mCandyMachine.printstate();

        mCandyMachine.insertCoin();
        mCandyMachine.printstate();

        mCandyMachine.turnCrank();

        mCandyMachine.printstate();

        mCandyMachine.insertCoin();
        mCandyMachine.printstate();

        mCandyMachine.turnCrank();

        mCandyMachine.printstate();
    }
}
