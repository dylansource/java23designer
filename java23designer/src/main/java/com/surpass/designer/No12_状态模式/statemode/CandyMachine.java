package com.surpass.designer.No12_状态模式.statemode;

/**
 * 糖果机
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/28 23:45
 */
public class CandyMachine {
    final static int SOLD_OUT_STATE = 0; //售罄
    final static int ON_READY_STATE = 1; //待机
    final static int HAS_COIN = 2; //放入硬币
    final static int SOLD_STATE = 3; //出售中

    private int state = SOLD_OUT_STATE;//初始化售罄状态
    private int count = 0; //糖果个数

    public CandyMachine(int count) {
        this.count = count;
        if (count > 0) {
            state = ON_READY_STATE;//待机状态
        }
    }

    //放入硬币
    public void insertCoin() {
        switch (state) {
            case SOLD_OUT_STATE:
                System.out.println("售罄,不能放硬币");
                break;
            case ON_READY_STATE:
                state = HAS_COIN;
                System.out.println("放入硬币成功,请转动把手");
                break;
            case HAS_COIN:
                System.out.println("已有硬币!不能再放入硬币");
                break;
            case SOLD_STATE:
                System.out.println("等待中,掉出糖果中");
                break;
        }
    }

    //退还硬币
    public void returnCoin() {
        switch (state) {
            case SOLD_OUT_STATE:
                System.out.println("机器售罄状态,无法退还!");
                break;
            case ON_READY_STATE:
                System.out.println("都没放硬币就想退钱???");
                break;
            case HAS_COIN:
                System.out.println("退回硬币中.....");
                state = ON_READY_STATE;
                break;
            case SOLD_STATE:
                System.out.println("已经转动把手,机器正在出售,无法退回!");
                break;
        }
    }

    //转动把手
    public void turnCrank() {
        switch (state) {
            case SOLD_OUT_STATE:
                System.out.println("成功转动把手,但是里面没有糖果了");
                break;
            case ON_READY_STATE:
                System.out.println("里面无硬币,转动把手失败!");
                break;
            case HAS_COIN:
                System.out.println("转动把手中");
                state = SOLD_STATE;
                dispense();
                break;
            case SOLD_STATE:
                System.out.println("正在掉出糖果,你转多了也没啥卵用!");
                break;
        }
    }

    //掉出糖果
    public void dispense() {
        count = count - 1;
        System.out.println("一个糖果正在掉出");
        if (count > 0) {
            state = ON_READY_STATE;
        } else {
            System.out.println("没有糖果了");
            state = SOLD_OUT_STATE;
        }
    }

    //打印状态
    public void printstate() {
        switch (state) {
            case SOLD_OUT_STATE:
                System.out.println("***机器处于售罄状态***");
                break;
            case ON_READY_STATE:
                System.out.println("***机器处于待机状态***");
                break;
            case HAS_COIN:
                System.out.println("***机器处于有硬币状态***");
                break;
            case SOLD_STATE:
                System.out.println("***机器处于出售状态***");
                break;
        }
    }
}
