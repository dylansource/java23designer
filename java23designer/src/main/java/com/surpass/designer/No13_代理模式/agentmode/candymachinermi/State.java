package com.surpass.designer.No13_代理模式.agentmode.candymachinermi;

import java.io.Serializable;

/**
 * State状态类,序列化
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/2 22:44
 */
public interface State extends Serializable {
    public void insertCoin();
    public void returnCoin();
    public void trunCrank();
    public void dispense(); //分发糖果
    public void printstate();
}
