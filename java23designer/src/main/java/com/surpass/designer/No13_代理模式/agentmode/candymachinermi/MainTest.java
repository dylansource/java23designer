package com.surpass.designer.No13_代理模式.agentmode.candymachinermi;

import java.rmi.Naming;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/2 23:07
 */
public class MainTest {
    public static void main(String[] args) {
        Monitor mMonitor = new Monitor();

        try {
            CandyMachineRemote mCandMachine = (CandyMachineRemote) Naming.lookup("rmi://localhost:6602/test1");
            mMonitor.addMachine(mCandMachine);
            mCandMachine = (CandyMachineRemote) Naming.lookup("rmi://localhost:6602/test2");
            mMonitor.addMachine(mCandMachine);
        } catch (Exception e) {
            e.printStackTrace();
        }
        mMonitor.report();
    }
}
