package com.surpass.designer.No13_代理模式.agentmode.candymachinermi;

import java.rmi.RemoteException;
import java.util.ArrayList;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/2 22:57
 */
public class Monitor {
    //本地代理
    private ArrayList<CandyMachineRemote> candyMachinelst;

    public Monitor() {
        candyMachinelst = new ArrayList<>();
    }

    public void addMachine(CandyMachineRemote mCandyMachine) {
        candyMachinelst.add(mCandyMachine);
    }

    //监控情况
    public void report() {
        CandyMachineRemote mCandyMachine;
        for (int i = 0; i < candyMachinelst.size(); i++) {
            mCandyMachine = candyMachinelst.get(i);
            try {
                System.out.println("Machine Loc:" + mCandyMachine.getLocation());
                System.out.println("Machine Candy count:" + mCandyMachine.getCount());
                System.out.print("Machine State:");
                try {
                    mCandyMachine.getState().printstate();
                } catch (Exception e) {
                    System.out.println();
                }
                mCandyMachine.getState().printstate();
            } catch (RemoteException e) {
                e.printStackTrace();
            }

        }
    }
}
