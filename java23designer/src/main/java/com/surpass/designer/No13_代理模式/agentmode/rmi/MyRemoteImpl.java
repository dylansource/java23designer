package com.surpass.designer.No13_代理模式.agentmode.rmi;

import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;

/**
 * 服务端
 * UnicastRemoteObject:通信类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/2 20:14
 */
public class MyRemoteImpl extends UnicastRemoteObject implements MyRemote {

    protected MyRemoteImpl() throws RemoteException {
        super();
    }

    @Override
    public String sayHello() throws RemoteException {
        return "Hello World";
    }

    public static void main(String[] args) {
        try {
            //System.setProperty("java.rmi.server.hostname", "192.168.1.2");
            //启动服务
            MyRemote service = new MyRemoteImpl();
            //设置端口
            LocateRegistry.createRegistry(6600);
            //绑定
            Naming.rebind("rmi://localhost:6600/RemoteHello",service);
            System.out.println("远程对象绑定成功!");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }
}
