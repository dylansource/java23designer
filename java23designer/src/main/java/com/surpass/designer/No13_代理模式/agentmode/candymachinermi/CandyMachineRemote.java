package com.surpass.designer.No13_代理模式.agentmode.candymachinermi;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface CandyMachineRemote extends Remote {
    public String getLocation() throws RemoteException;

    public int getCount() throws RemoteException;

    public State getState() throws RemoteException;

}
