package com.surpass.designer.No13_代理模式.agentmode;

import com.surpass.designer.No13_代理模式.agentmode.candymachine.CandyMachine;

import java.util.ArrayList;

/**
 * 糖果机项目监控类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 23:13
 */
public class Monitor {
    //糖果机集合
    private ArrayList<CandyMachine> candyMachinelst;

    public Monitor() {
        candyMachinelst = new ArrayList<>();
    }

    public void addMachine(CandyMachine mCandyMachine) {
        candyMachinelst.add(mCandyMachine);
    }

    //监控情况
    public void report() {
        CandyMachine mCandyMachine;
        for (int i = 0; i < candyMachinelst.size(); i++) {
            mCandyMachine = candyMachinelst.get(i);
            System.out.println("Machine Loc:" + mCandyMachine.getLocation());
            System.out.println("Machine Candy count:" + mCandyMachine.getCount());
            System.out.print("Machine State:");
            mCandyMachine.printstate();
            //System.out.println("Machine State:" + mCandyMachine.getState().getstatename());
        }
    }
}
