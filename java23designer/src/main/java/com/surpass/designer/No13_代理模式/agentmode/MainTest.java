package com.surpass.designer.No13_代理模式.agentmode;

import com.surpass.designer.No13_代理模式.agentmode.candymachine.CandyMachine;

/**
 * 糖果机监控项目
 * 你是公司上市老板:需要知道各个地方的糖果机运行情况,监控运行
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 23:17
 */
public class MainTest {
    public static void main(String[] args) {
        Monitor mMonitor = new Monitor();
        CandyMachine mCandyMachine = new CandyMachine("NY", 6);
        mMonitor.addMachine(mCandyMachine);

        mCandyMachine = new CandyMachine("TK", 4);
        mCandyMachine.insertCoin();
        mMonitor.addMachine(mCandyMachine);

        mCandyMachine = new CandyMachine("Bj", 14);
        mCandyMachine.insertCoin();
        mCandyMachine.turnCrank();
        mMonitor.addMachine(mCandyMachine);

        mMonitor.report();
    }
}
