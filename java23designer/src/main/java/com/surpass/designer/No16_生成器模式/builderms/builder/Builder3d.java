package com.surpass.designer.No16_生成器模式.builderms.builder;

/**
 * 3天度假计划
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 22:36
 */
public class Builder3d extends AbsBuilder{

    public Builder3d(String std) {
        super(std);
    }

    @Override
    public void buildDay(int i) {
        mVacation.setVacationDay(i);
    }

    @Override
    public void addHotel(String hotel) {
        mVacation.setHotel(hotel);
    }

    @Override
    public void addTicket(String ticket) {
        mVacation.addTicket(ticket);
    }

    @Override
    public void addEvent(String event) {
        mVacation.addEvent(event);
    }

    @Override
    public void buildvacation() {
        addTicket("Plane Ticket");
        addEvent("Fly to Destination");
        addEvent("Supper");
        addEvent("Dancing");
        addHotel("Four Seasons");

        //第二天
        mVacation.addDay();
        addTicket("Theme Park");
        addEvent("Bus to Park");
        addEvent("lunch");
        addHotel("Four Seasons");

        //第三天
        mVacation.addDay();
        addTicket("Plane Ticket");
        addEvent("City Tour");
        addEvent("Fly to Home");

    }
}
