package com.surpass.designer.No20_中介者模式.mediator;

/**
 * 同事窗帘
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 21:36
 */
public class Curtains extends Colleague {

    public Curtains(Mediator mediator, String name) {
        super(mediator, name);
        mediator.Register(name,this);
    }

    @Override
    public void SendMessage(int stateChange) {
        this.GetMediator().GetMessage(stateChange, this.name);
    }

    public void UpCurtains() {
        System.out.println("I am holding Up Curtains!");

    }
}
