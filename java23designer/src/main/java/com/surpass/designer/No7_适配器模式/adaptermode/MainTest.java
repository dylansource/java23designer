package com.surpass.designer.No7_适配器模式.adaptermode;

import com.surpass.designer.No7_适配器模式.adaptermode.adapter.TurkeyAdapter2;
import com.surpass.designer.No7_适配器模式.adaptermode.duck.Duck;
import com.surpass.designer.No7_适配器模式.adaptermode.duck.GreenHeadDuck;
import com.surpass.designer.No7_适配器模式.adaptermode.turkey.WildTurkey;

/**
 * 模拟适配器:火鸡变鸭子
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/10 22:03
 */
public class MainTest {
    public static void main(String[] args) {
        GreenHeadDuck duck = new GreenHeadDuck();

        WildTurkey turkey = new WildTurkey();

        //对象适配器产生的鸭子
        //Duck duck2turkeyAdapter = new TurkeyAdapter(turkey);
        //类适配器产生的鸭子
        Duck duck2turkeyAdapter = new TurkeyAdapter2();
        //火鸡的行为
        turkey.gobble();
        turkey.fly();
        //鸭子的行为
        duck.quack();
        duck.fly();

        //适配器模拟的鸭子
        duck2turkeyAdapter.quack(); //火鸡叫声
        duck2turkeyAdapter.fly(); // 火鸡飞行
    }
}
