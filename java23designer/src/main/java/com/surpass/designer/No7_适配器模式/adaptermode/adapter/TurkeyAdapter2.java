package com.surpass.designer.No7_适配器模式.adaptermode.adapter;

import com.surpass.designer.No7_适配器模式.adaptermode.duck.Duck;
import com.surpass.designer.No7_适配器模式.adaptermode.turkey.WildTurkey;

/**
 * 类适配器火鸡模拟鸭子
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/10 22:27
 */
public class TurkeyAdapter2 extends WildTurkey implements Duck {


    @Override
    public void quack() {
        super.gobble();
    }

    @Override
    public void fly() {
        super.fly();
        super.fly();
        super.fly();
    }
}
