package com.surpass.designer.No7_适配器模式.adaptermode.duck;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/10 21:58
 */
public class GreenHeadDuck implements Duck {
    @Override
    public void quack() {
        System.out.println("Ga Ga");
    }

    @Override
    public void fly() {
        System.out.println("I am flying a long distance");
    }
}
