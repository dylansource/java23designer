package com.surpass.designer.No5_工厂模式.pizzastore.pizza;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 15:20
 */
public abstract class Pizza {
    protected String name;

    public abstract void prepare();
    public void bake(){
        System.out.println(name + " baking");
    }
    public void cut(){
        System.out.println(name + " cutting");
    }
    public void box(){
        System.out.println(name + " boxing");
    }

    public void setName(String name) {
        this.name = name;
    }
}
