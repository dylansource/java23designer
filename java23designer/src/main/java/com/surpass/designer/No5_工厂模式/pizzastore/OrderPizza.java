package com.surpass.designer.No5_工厂模式.pizzastore;

import com.surpass.designer.No5_工厂模式.pizzastore.pizza.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 传统设计方案,每增加一种pizza,就需要新建类,新增ifelse
 * 修改开放,存在问题
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 15:25
 */
public class OrderPizza {
    public OrderPizza(){
        Pizza pizza = null;
        String ordertype;

        do {
            ordertype = gettype();
            if (ordertype.equals("cheese")) {
                pizza = new CheesePizza();
            } else if (ordertype.equals("greek")) {
                pizza = new GreekPizza();
            } else if (ordertype.equals("pepper")) {
                pizza = new PepperPizza();
            } else if(ordertype.equals("chinese")){//新增了ChinesePizza,需要新增elseif,整个类需要重新编译
                pizza = new ChinesePizza();
            } else {
                break;
            }
            pizza.prepare();
            pizza.bake();
            pizza.cut();
            pizza.box();
        } while (true);

    }

    private String gettype() {
        try {
            BufferedReader strin = new BufferedReader(new InputStreamReader(
                    System.in
            ));
            System.out.println("input pizza type:");
            String str = strin.readLine();
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
