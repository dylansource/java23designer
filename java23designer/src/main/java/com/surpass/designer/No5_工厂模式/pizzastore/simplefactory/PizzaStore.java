package com.surpass.designer.No5_工厂模式.pizzastore.simplefactory;

/**
 * 店铺扩张多个地方  需要传入多个工厂对象
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 15:32
 */
public class PizzaStore {
    public static void main(String[] args) {
        //A地的工厂
        SimplePizzaFactory simplePizzaFactory1 = new SimplePizzaFactory();
        //B地的工厂
        SimplePizzaFactory simplePizzaFactory2 = new SimplePizzaFactory();
        OrderPizza mOrderPizza;
        //通过简单工厂加工实现要求
        mOrderPizza = new OrderPizza(simplePizzaFactory1);
    }
}
