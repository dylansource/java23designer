package com.surpass.designer.No5_工厂模式.pizzastore.absfactory;

import com.surpass.designer.No5_工厂模式.pizzastore.pizza.Pizza;

/**
 * 抽象工厂
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 16:21
 */
public interface AbsFactory {
    public Pizza CreatePizza(String ordertype);
}
