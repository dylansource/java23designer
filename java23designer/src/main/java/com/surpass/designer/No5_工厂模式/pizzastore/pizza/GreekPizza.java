package com.surpass.designer.No5_工厂模式.pizzastore.pizza;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 15:22
 */
public class GreekPizza extends Pizza {
    @Override
    public void prepare() {
        super.setName("GreekPizza");
        System.out.println(name + " preparing");
    }
}
